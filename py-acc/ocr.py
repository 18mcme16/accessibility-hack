import pytesseract
from PIL import Image

def return_test(image_path):
    return pytesseract.image_to_string(Image.open(image_path))

