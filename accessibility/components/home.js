import React, { Component } from "react";
import { Alert, Button, StyleSheet, Text, View } from "react-native";

class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Seeing AI</Text>
        <Button title="Start" onPress={() => Alert.alert("Start")} />
        <Button title="Help" onPress={() => Alert.alert("Help")} />
      </View>
    );
  }
}

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "#66ff66",
    backgroundColor: '#fff'
  },
  button: {},
});
